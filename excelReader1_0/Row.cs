﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace excelReader1_0
{
    public class Row
    {
        public String title { get; set; }
        public String PList { get; set; }
        public String Link { get; set; }
        public String InEx { get; set; }
        
        public Row(List<string> header, String[] rows)
        {
            String[] linkPuzzle = rows[5].Split('.');
            string Puzzle = "";

            //handle the .zip and .P2G problem 
            foreach(string s in linkPuzzle){
                if (s != "zip")
                {
                    if (s == "P2G")
                    {
                        Puzzle += "." + s; // re-add the '.' taken out in the split 
                    }
                    else
                    {
                        Puzzle += s;
                    }
                }
                rows[5] = Puzzle;
            }

            title = rows[4];
            PList = rows[0] +formatString(rows[1],rows[2], rows[3]);
            Link = "https://chpweb.cmich.edu/mediasite/Internal/" + rows[5] +"/Player.html";
            InEx = "Internal";

            // https://chpweb.cmich.edu/mediasite/External/Activity_Intensity_of_Inclusive_and_Non-Inclusive/Player.html
        }
        public String formatString(String a, String b, String c)
        {
            String[] format = {a,b,c};
            String isGood = "";
            for (int i = 0; i < 3; i++)
            {
                if (!format[i].Equals("") || !format[i].Equals(" "))
                {
                    isGood += ", "+ format[i];
                }
 
            }
            return null;
        }
    }
     

}
