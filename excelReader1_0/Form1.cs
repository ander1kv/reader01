﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;


namespace excelReader1_0
{
    public partial class Form1 : Form
    {

        //global attributes 
        //dev
        //private static string _connectionString = "Trusted_Connection=yes; server=10.209.19.23,1434\\htgdev; database=data";
        //production
        private static string _connectionString = "Data Source=chp-db1;Initial Catalog=movies;Integrated Security=True";//"Trusted_Connection=yes; server=chp-db1,1434\\HTGDEV; database=movies;";
  
        public List<Row> Rows = new List<Row>();
        public List<string> header;

        string Rfilepath = "C:/Users/ander1kv/Desktop/internal.csv";
        //string Rfilepath = "C:/Users/ander1kv/Desktop/internal.csv";

        //"C://Users/ander1kv/Documents/testRead.csv";
        //string WfilePath = "C://Users/ander1kv/Documents/newFile.csv";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //read in header 
            try
            {
                StreamReader sr;
                sr = new StreamReader(Rfilepath);

                string line = sr.ReadLine();
                string[] value = line.Split(',');

                header = value.ToList();
                //MessageBox.Show(header.ElementAt(0) + header.ElementAt(1) + header.ElementAt(2) + header.ElementAt(3) + header.ElementAt(4) + header.ElementAt(5));
                //read in rows
                while (!sr.EndOfStream)
                {                 
                        value = sr.ReadLine().Split(',');
                    
                    foreach (string s in value)
                    {
                        s.Trim();
                    }
                    if (value.Length >= header.Count())
                    {
                        for(int i =0; i < value.Length; i++)
                        {
                            StringBuilder sb = new StringBuilder(value[i]);

                            for (int j = 0; j < value[i].Length; j++)
                            {
                                if (value[i].ElementAt(j).Equals('*'))
                                {
                                    sb[j] = ',';                                   
                                }
                                
                            }
                           
                            value[i] = sb.ToString();
                        }
                        this.Rows.Add(new Row(header, value));                   
                    }
                }
                MessageBox.Show("File Read");
            }
            catch (Exception ex0)
            {

                string exept = (" Exception Type: " + ex0.GetType());
                string message = ("   Message: {0}" + ex0.Message);
                MessageBox.Show(exept+"'::, "+message);
               

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
             
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                
                foreach (var row in Rows)
                {
                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction = connection.BeginTransaction("transaction_1");


                // Assign transaction object and connection to 
                // Command object for pending local transaction.
                command.Connection = connection;
                command.Transaction = transaction;
                    command.CommandType = System.Data.CommandType.Text;
                    command.Connection = connection;

                    command.CommandText = @"INSERT INTO MovieList VALUES (@movie, @presList, @InEx, @Link)";
                //int k = 1;
               
                    

                    //command.Parameters.AddWithValue("@id", k);
                    checkNullSqlParameter(command, row.title, "movie");
                    checkNullSqlParameter(command, row.PList, "presList");
                    checkNullSqlParameter(command, row.InEx, "InEx");
                    checkNullSqlParameter(command, row.Link, "Link");
                    
                    command.ExecuteNonQuery();
                    //k++;
                       
               
                transaction.Commit();   
                // attempt to commit transaction.  
                
                
                }
                connection.Close();
                MessageBox.Show("All Records are written to the database.");
            }
        }
        private static void checkNullSqlParameter(SqlCommand command, String applicantAttribute, String cmdText)
        {
            if (applicantAttribute == " " || applicantAttribute == "")
            {
                command.Parameters.AddWithValue("@" + cmdText, DBNull.Value);
            }
            else
            {
                command.Parameters.AddWithValue("@" + cmdText, applicantAttribute);
            }
        }
       
    }
}
